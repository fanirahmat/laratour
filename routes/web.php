<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TempatWisataController;
use App\Http\Controllers\PaketController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\PesertaController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::middleware(['auth'])->group(function () {
   // Tempat Wisata
    Route::get('/tempat_wisata',[TempatWisataController::class,'showTempatWisata'])->name('tempat-wisata-view');
    Route::get('/tempat_wisata/{id}',[TempatWisataController::class,'showTempatWisataByID'])->name('tempat-wisata-view-byid');
    Route::get('/tempat_wisata/create/form',[TempatWisataController::class,'showFormCreateTempatWisata'])->name('create-tempat-wisata-view');
    Route::post('/tempat_wisata/create',[TempatWisataController::class,'createTempatWisata'])->name('create-tempat-wisata');
    Route::post('/tempat_wisata/edit/{id}',[TempatWisataController::class,'editTempatWisata'])->name('edit-tempat-wisata');
    Route::delete('/tempat_wisata/delete/{id}',[TempatWisataController::class,'deleteTempatWisata'])->name('delete-tempat-wisata');

    // Paket
    Route::get('/paket',[PaketController::class,'showPaket'])->name('paket-view');
    Route::get('/paket/{id}',[PaketController::class,'showPaketByID'])->name('paket-view-byid');
    Route::get('/paket/create/form',[PaketController::class,'showFormCreatePaket'])->name('create-paket-view');
    Route::post('/paket/create',[PaketController::class,'createPaket'])->name('create-paket');
    Route::post('/paket/edit/{id}',[PaketController::class,'editPaket'])->name('edit-paket');
    Route::delete('/paket/delete/{id}',[PaketController::class,'deletePaket'])->name('delete-paket');

    // Transkasi
    Route::get('/transaksi',[TransaksiController::class,'showTransaksi'])->name('transaksi-view');
    Route::get('/transaksi/{id}',[TransaksiController::class,'showTransaksiByID'])->name('transaksi-view-byid');
    Route::get('/transaksi/create/form/{id}',[TransaksiController::class,'showFormCreateTransaksi'])->name('create-transaksi-view');
    Route::post('/transaksi/create/{id}',[TransaksiController::class,'createTransaksi'])->name('create-transaksi');
    Route::put('/transaksi/confirm/{id}',[TransaksiController::class,'confirmTransaksi'])->name('confirm-transaksi');

    // Peserta
    Route::get('/peserta/create/form/{id}',[PesertaController::class,'showFormCreatePeserta'])->name('create-peserta-view');
    Route::get('/peserta/{id}',[PesertaController::class,'showPesertaByID'])->name('peserta-view-byid');
    Route::post('/peserta/create/{id}',[PesertaController::class,'createPeserta'])->name('create-peserta');
    Route::post('/peserta/edit/{id}',[PesertaController::class,'editPeserta'])->name('edit-peserta');
    Route::delete('/peserta/delete/{id}',[PesertaController::class,'deletePeserta'])->name('delete-peserta');
});