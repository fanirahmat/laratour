<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TempatWisata;

class Paket extends Model
{
    use HasFactory;

    protected $table = "pakets";
    protected $fillable = [
        'name',
        'desc',
        'tempat_wisata_id',
        'price_per_person',
    ];

    public function tempat_wisatas() {
        return $this->belongsTo(TempatWisata::class, 'tempat_wisata_id', 'id');
    }

}
