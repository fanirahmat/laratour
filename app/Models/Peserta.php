<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi;

class Peserta extends Model
{
    use HasFactory;
    protected $table = "pesertas";
    protected $fillable = [
        'name',
        'email',
        'transaksi_id',
    ];

    public function transaksis() {
        return $this->belongsTo(Transaksi::class, 'transaksi_id', 'id');
    }
}
