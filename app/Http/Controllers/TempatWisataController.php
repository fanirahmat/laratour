<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TempatWisata;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class TempatWisataController extends Controller
{
    public function showTempatWisata()
    {
        return view('tempat_wisata.tempat_wisata')->with([
                'tempat_wisata' => TempatWisata::all(),
                'title' => 'Daftar Tempat Wisata'
            ]);
    }

    public function showTempatWisataByID($id) {
        return view('tempat_wisata.form_tempat_wisata')->with([
            'tempat_wisata' => TempatWisata::find($id)
        ]);
    }

    public function showFormCreateTempatWisata()
    {
        return view('tempat_wisata.form_tempat_wisata');
    }

    public function createTempatWisata(Request $request) {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'photo' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'address' => ['required', 'string', 'max:255'],
            'penginapan' => ['required', 'string', 'max:255'],
        ]);

        $imageName = $request->photo->getClientOriginalName();  
        $imagePath = 'images/'. time() . '.'.$imageName;
        $isImageUploaded = Storage::disk('public')->put($imagePath, file_get_contents($request->photo));

        if ($isImageUploaded) {
            TempatWisata::create([
                'nama' => $request['name'],
                'desc' => $request['desc'],
                'photo' => $imagePath,
                'address' => $request['address'],
                'penginapan' => $request['penginapan'],
            ]);
        }

        return to_route('tempat-wisata-view');
    }

    public function editTempatWisata(Request $request, $id) {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'photo' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'address' => ['required', 'string', 'max:255'],
            'penginapan' => ['required', 'string', 'max:255'],
        ]);

        $imageName = $request->photo->getClientOriginalName();  
        $imagePath = 'images/'. time() . '.'.$imageName;
        $isImageUploaded = Storage::disk('public')->put($imagePath, file_get_contents($request->photo));

        if ($isImageUploaded) {
            $tw = TempatWisata::find($id);
            $tw->nama = $request->name;
            $tw->desc = $request->desc;
            $tw->photo = $imagePath;
            $tw->address = $request->address;
            $tw->penginapan = $request->penginapan;
            $tw->save();
        }

        return to_route('tempat-wisata-view');
    }

    public function deleteTempatWisata($id) {
        $tw = TempatWisata::find($id);
        $imagePath = "public/".$tw->photo; 
        if (Storage::exists($imagePath)) {
            Storage::delete($imagePath);
        }
        $tw->delete();

        return back();
    }
}
