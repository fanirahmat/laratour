<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peserta;

class PesertaController extends Controller
{

    public function showFormCreatePeserta($transaksi_id) {
        return view('peserta.form_peserta')->with([
            'transaksi_id' => $transaksi_id,
            'title' => "Tambah Peserta"
        ]);
    }

    public function showPesertaByID($id) {
        return view('peserta.form_peserta')->with([
            'peserta' => Peserta::find($id),
            'title' => "Edit Peserta"
        ]);
    }

    public function createPeserta(Request $request, $transaksi_id) {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        Peserta::create([
            'name' => $request->name,
            'email' => $request->email,
            'transaksi_id' => $transaksi_id,
        ]);

        return to_route('transaksi-view-byid', $transaksi_id);
    }

    public function editPeserta(Request $request, $id) {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $peserta = Peserta::find($id);
        $peserta->name = $request->name;
        $peserta->email = $request->email;
        $peserta->save();

        return to_route('transaksi-view-byid', $peserta->transaksi_id);
    }

    public function deletePeserta($id) {
        $peserta = Peserta::find($id);
        $peserta->delete();

        return back();
    }
}
