<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paket;
use App\Models\TempatWisata;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class PaketController extends Controller
{
    public function showPaket()
    {
        return view('paket.paket')->with([
                'paket' => Paket::all(),
                'title' => 'Daftar Paket Wisata'
            ]);
    }

    public function showPaketByID($id) {
        return view('paket.form_paket')->with([
            'paket' => Paket::find($id),
            'tempat_wisata' => TempatWisata::all()
        ]);
    }

    public function showFormCreatePaket()
    {
        return view('paket.form_paket')->with([
            'tempat_wisata' => TempatWisata::all()
        ]);
    }

    public function createPaket(Request $request) {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'tempat_wisata_id' => ['required', 'integer'],
            'price_per_person' => ['required', 'integer'],
        ]);

        Paket::create([
            'name' => $request['name'],
            'desc' => $request['desc'],
            'tempat_wisata_id' => $request['tempat_wisata_id'],
            'price_per_person' => $request['price_per_person'],
        ]);

        return to_route('paket-view');
    }

    public function editPaket(Request $request, $id) {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'tempat_wisata_id' => ['required', 'integer'],
            'price_per_person' => ['required', 'integer'],
        ]);

        $paket = Paket::find($id);

        $paket->name = $request->name;
        $paket->desc = $request->desc;
        $paket->tempat_wisata_id = $request->tempat_wisata_id;
        $paket->price_per_person = $request->price_per_person;
        $paket->save();

        // $paket::update([
        //     'name' => $request['name'],
        //     'desc' => $request['desc'],
        //     'tempat_wisata_id' => $request['tempat_wisata_id'],
        //     'price_per_person' => $request['price_per_person'],
        // ]);

        return to_route('paket-view');
    }

    public function deletePaket($id) {
        $tw = Paket::find($id);
        $tw->delete();

        return back();
    }
}
