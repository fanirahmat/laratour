<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\Paket;
use App\Models\Peserta;

class TransaksiController extends Controller
{
    public function showTransaksi()
    {
        $user = Auth::user();
        if ($user->roles->id == 2) {
            // customer
            $id = $user->id;
            $transaksi = Transaksi::where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        } else {
             // admin
            $transaksi = Transaksi::orderBy('created_at', 'DESC')->get();
        }

        return view('transaksi.transaksi')->with([
            'transaksi' => $transaksi,
            'title' => 'Data Transaction'
        ]);
    }

    public function showTransaksiByID($id) {
        $peserta = Peserta::where('transaksi_id', $id)->orderBy('created_at', 'ASC')->get();
        $transaksi = Transaksi::find($id);

        $amount_days = $transaksi->amount_days;
        $price = $transaksi->pakets->price_per_person;
        $amount_person = $peserta->count();

        $total = ($price * $amount_person) * $amount_days;
        return view('transaksi.transaksi_detail')->with([
            'transaksi' => $transaksi,
            'peserta' => $peserta,
            'title' => 'Booking Detail',
            'total' => $total
        ]);
    }

    public function showFormCreateTransaksi($paket_id)
    {
        return view('transaksi.form_transaksi')->with([
            'paket_id' => $paket_id,
        ]);
    }

    public function createTransaksi(Request $request, $paket_id) {
        $request->validate([
            'booking_date' => ['required', 'date'],
            'amount_days' => ['required', 'integer'],
        ]);

        $user = Auth::user();
        $user_id = $user->id;
        $amount_days = $request['amount_days'];
       
        $transaksi = Transaksi::create([
            'user_id' => $user_id,
            'paket_id' => $paket_id,
            'booking_date' => $request['booking_date'],
            'amount_days' => $amount_days,
            'status' => 'pending'
        ]);

        if ($transaksi) {
            Peserta::create([
                'name' => $user->name,
                'email' => $user->email,
                'transaksi_id' => $transaksi->id,
            ]);
        }

        return to_route('transaksi-view');
    }

    public function confirmTransaksi($id) {
        $transaksi = Transaksi::find($id);

        // update status transaksi
        $transaksi->update([
            'status' => 'accepted'
        ]);

        return back();
    }
    
}
