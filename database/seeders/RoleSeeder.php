<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // ID
        // 1 => admin
        // 2 => customer
        $data = [
            [
                'name' => 'admin', 
            ],
            [
                'name' => 'customer',
            ],
        ];
        Role::insert($data);
    }
}
