<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesertas', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->foreignId('transaksi_id');
            $table->timestamps();

            $table->foreign('transaksi_id')->references('id')->on('transaksis');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pesertas');
    }
};
