@extends('layouts.app')

@section('content')
<div class="container">
  <a href="{{ route('create-paket-view') }}" class="btn btn-success btn-sm">Add</a>

    <div class="row">
       @foreach ($paket as $no => $hasil)
        <div class="col-md-3">
            <div class="card">
                <img src="{{ asset('/storage/'.$hasil->tempat_wisatas->photo) }}" class="card-img-top" alt="..." style= "height: 250px;">
               
                <div class="card-body">
                  <h5 class="card-title">{{ $hasil->name ?? "" }}</h5>
                  <p class="card-text">Rp{{ $hasil->price_per_person ?? "" }}</p>
                  <p class="card-text">{{ $hasil->desc ?? "" }}</p>
                  <p class="card-text">Penginapan: {{ $hasil->tempat_wisatas->penginapan ?? "" }}</p>

                  @if (Auth::user()->roles->id == 1)
                    <a href="{{ route('paket-view-byid', $hasil->id) }}" class="btn btn-primary">Edit</a>
                    <form action="{{ route('delete-paket', $hasil->id) }}" method="POST">
                          @csrf
                          @method('delete')
                          <button class="btn btn-danger">Delete</button>
                    </form>
                  @elseif (Auth::user()->roles->id == 2)
                    <a href="{{ route('create-transaksi-view', $hasil->id) }}" class="btn btn-info">Book Now</a>
                  @endif
                  
                </div>
              </div>
        </div>
        @endforeach
        
        
    </div>
</div>
@endsection
