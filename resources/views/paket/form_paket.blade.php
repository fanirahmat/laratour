@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Paket</div>

                <div class="card-body">
                   
                    @isset($paket)
                        <form method="POST" action="{{ route('edit-paket', $paket->id) }}" enctype="multipart/form-data">
                    @else
                        <form method="POST" action="{{ route('create-paket') }}" enctype="multipart/form-data">
                    @endisset
                    {{-- <form method="POST" action="{{ route('create-paket') }}" enctype="multipart/form-data"> --}}

                        @csrf
                        {{-- Name --}}
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Name</label>

                            <div class="col-md-6">
                                <input value="{{ $paket->name ?? "" }}" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Desc --}}
                        <div class="row mb-3">
                            <label for="desc" class="col-md-4 col-form-label text-md-end">Description</label>

                            <div class="col-md-6">
                                <input value="{{ $paket->desc ?? "" }}" id="desc" type="text" class="form-control @error('desc') is-invalid @enderror" name="desc" required autocomplete="desc" autofocus>

                                @error('desc')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                
                        {{-- price per person --}}
                        <div class="row mb-3">
                            <label for="price_per_person" class="col-md-4 col-form-label text-md-end">Price / person</label>

                            <div class="col-md-6">
                                <input value="{{ $paket->price_per_person ?? 0 }}" id="price_per_person" type="number" class="form-control @error('price_per_person') is-invalid @enderror" name="price_per_person" required autocomplete="price_per_person">
                                @error('price_per_person')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                         {{-- tempat wisata --}}
                         <div class="row mb-3">
                            <label for="tempat_wisata_id" class="col-md-4 col-form-label text-md-end">Tempat Wisata</label>
                            <div class="col-md-6">
                                <select id="tempat_wisata_id" name="tempat_wisata_id" class="custom-select form-control @error('tempat_wisata_id') is-invalid @enderror" required="required">
                                    @foreach  ($tempat_wisata as $no => $hasil)

                                    @isset($paket)
                                        <option value="{{ $hasil->id }}"
                                            @if ($paket->tempat_wisata_id == $hasil->id)
                                                {{'selected="selected"'}}
                                            @endif >
                                            {{ $hasil->nama }}
                                        </option>
                                    @else
                                        <option value="{{ $hasil->id }}">
                                            {{ $hasil->nama }}
                                        </option>
                                    @endisset

                                    



                                    @endforeach
                                  </select>

                                @error('tempat_wisata_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                         </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
