@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{ route('create-tempat-wisata-view') }}" class="btn btn-success btn-sm">Add</a>
    <div class="row">
        @foreach ($tempat_wisata as $no => $hasil)
        <div class="col-md-3">
            <div class="card">
                <img src="{{ asset('/storage/'.$hasil->photo) }}" class="card-img-top" alt="..." style= "height: 250px;">
               
                <div class="card-body">
                  <h5 class="card-title">{{ $hasil->nama ?? "" }}</h5>
                  <p class="card-text">{{ $hasil->desc ?? "" }}</p>
                  <a href="{{ route('tempat-wisata-view-byid', $hasil->id) }}" class="btn btn-primary">Edit</a>
                  <form action="{{ route('delete-tempat-wisata', $hasil->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <button class="btn btn-danger">Delete</button>
                    </form>
                </div>
              </div>
        </div>
        @endforeach

        
        
        
    </div>
</div>
@endsection
