@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tempat Wisata</div>

                <div class="card-body">
                    @isset($tempat_wisata)
                        <form method="POST" action="{{ route('edit-tempat-wisata', $tempat_wisata->id) }}" enctype="multipart/form-data">
                    @else
                        <form method="POST" action="{{ route('create-tempat-wisata') }}" enctype="multipart/form-data">
                    @endisset
                        @csrf
                        {{-- Name --}}
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Name</label>

                            <div class="col-md-6">
                                <input value="{{ $tempat_wisata->nama ?? "" }}" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Desc --}}
                        <div class="row mb-3">
                            <label for="desc" class="col-md-4 col-form-label text-md-end">Description</label>

                            <div class="col-md-6">
                                <input value="{{ $tempat_wisata->desc ?? "" }}" id="desc" type="text" class="form-control @error('desc') is-invalid @enderror" name="desc" required autocomplete="desc" autofocus>

                                @error('desc')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Photo --}}
                        <div class="row mb-3">
                            <label for="photo" class="col-md-4 col-form-label text-md-end">Photo</label>

                            <div class="col-md-6">
                                <input id="photo" type="file" class="form-control @error('photo') is-invalid @enderror" name="photo" required autocomplete="photo" autofocus>

                                @error('photo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Address --}}
                        <div class="row mb-3">
                            <label for="address" class="col-md-4 col-form-label text-md-end">Address</label>

                            <div class="col-md-6">
                                <input value="{{ $tempat_wisata->address ?? "" }}" id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" required autocomplete="address" autofocus>

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Penginapan --}}
                        <div class="row mb-3">
                            <label for="penginapan" class="col-md-4 col-form-label text-md-end">Penginapan</label>

                            <div class="col-md-6">
                                <input value="{{ $tempat_wisata->penginapan ?? "" }}" id="penginapan" type="text" class="form-control @error('penginapan') is-invalid @enderror" name="penginapan" required autocomplete="penginapan" autofocus>

                                @error('penginapan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection
