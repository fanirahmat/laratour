@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Booking</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('create-transaksi', $paket_id) }}" enctype="multipart/form-data">
                        @csrf

                        {{-- Booking date --}}
                        <div class="row mb-3">
                            <label for="booking_date" class="col-md-4 col-form-label text-md-end">Date</label>

                            <div class="col-md-6">
                                <input id="booking_date" type="date" class="form-control @error('booking_date') is-invalid @enderror" name="booking_date" required autocomplete="booking_date" autofocus>

                                @error('booking_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                      
                        {{-- amount days --}}
                        <div class="row mb-3">
                            <label for="amount_days" class="col-md-4 col-form-label text-md-end">Amount days</label>

                            <div class="col-md-6">
                                <input min="1" max="7" id="amount_days" type="number" class="form-control @error('amount_days') is-invalid @enderror" name="amount_days" required autocomplete="amount_days" autofocus>

                                @error('amount_days')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Confirm
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection
