@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $title ?? "" }}</div>
                <div class="card-body">
                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Paket</th>
                        <th scope="col">Date</th>
                        {{-- <th scope="col">Amount Person</th> --}}
                        <th scope="col">Amount Days</th>
                        {{-- <th scope="col">Total</th> --}}
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transaksi as $no => $hasil)
                        <tr>
                        <th scope="row">{{ $no+1 }}</th>
                        <td>{{ $hasil->users->name }}</td>
                        <td>{{ $hasil->pakets->name }}</td>
                        <td>{{ $hasil->booking_date }}</td>
                        {{-- <td>{{ $hasil->amount_person }}</td> --}}
                        <td>{{ $hasil->amount_days }} Days</td>
                        {{-- <td>Rp{{ $hasil->total }}</td> --}}
                        <td>{{ $hasil->status }}</td>
                        <td>
                            <a href="{{ route('transaksi-view-byid', $hasil->id) }}" class="btn btn-primary btn-sm">Detail</a>
                            
                        </td>
                        </tr>
                        @endforeach
                        
                       
                    </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
