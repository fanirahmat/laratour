@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $title ?? "" }}</div>
                <div class="card-body">
                    <h5 class="card-title">{{ $transaksi->pakets->name ?? "" }}</h5>
                    <p class="card-text">{{ $transaksi->booking_date ?? "" }}</p>
                    <p class="card-text">{{ $transaksi->amount_days ?? "" }} Days</p>
                    <p class="card-text">{{ $transaksi->status ?? "" }}</p>

                    <p class="card-text">Total : Rp{{ $total ?? "" }}</p>

                    @if (Auth::user()->roles->id == 1 && $transaksi->status == 'pending')
                                    <form action="{{ route('confirm-transaksi', $transaksi->id) }}" method="POST">
                                        @csrf
                                        @method('put')
                                        <button class="btn btn-info btn-sm">Confirm</button>
                                    </form>
                            @endif 
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Peserta</div>
                <div class="card-body">
                    @if (Auth::user()->roles->id == 2 && $transaksi->status == 'pending')
                    <a href="{{ route('create-peserta-view', $transaksi->id) }}" class="btn btn-success btn-sm">Add</a>
                    @endif 
                    
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($peserta as $no => $hasil)
                            <tr>
                            <th scope="row">{{ $no+1 }}</th>
                            <td>{{ $hasil->name }}</td>
                            <td>{{ $hasil->email }}</td>
                            
                            <td>
                                
                                @if (Auth::user()->roles->id == 2 && $transaksi->status == 'pending')
                                    <a href="{{ route('peserta-view-byid', $hasil->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                    <form action="{{ route('delete-peserta', $hasil->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                @endif 
                            </td>
                            </tr>
                            @endforeach
                            
                           
                        </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
